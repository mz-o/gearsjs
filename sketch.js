

// TODO experiment über url laden --> evtl danach auf /? umleiten, um nicht bei jedem neu laden gefragt zu werden - das exp. ist ja dann im storage
// TODO bruch für magic connection

const s = p => {
  let revolutionsPerMinute = 10;
  let rotationMethod = 'revolution'
  //let rotationMethod = 'hand'

  let driverGears = [];
  let allGears = [];
  let selectedGears = [];
  let selDist = null;
  let topSpace = 0;

  let renderer;

  let frameRate = 24;

  let showGearValue = false;
  let showConnectionValue = false;

  let isDark = false;

  Gear.isDark = isDark;
  


  function rotationRPM(rpm) {
    // Umdr. pro Minute
    Gear.rotation += rpm*(p.TWO_PI/frameRate)/60;   //0.002;
    if (!Number.isFinite(Gear.rotation)) {
      Gear.rotation = 0;
    }
  }




  function connect(g1, g2, con) {
    switch(con) {
      case 't': case 'touching':    g1.connectTouching(g2); break;
      case 'x': case 'crossed':     g1.connectCrossChained(g2); break;
      case 'c': case 'chained':     g1.connectChained(g2); break;
      case 's': case 'sticky':      g1.connectSticky(g2); break;
      case 'm': case 'magic':       g1.connectMagic(g2, new Bruch(1,1)); break; // TODO
    }
  }

  function create(data, last) {
    if (Array.isArray(data)) {
      if (typeof last === 'undefined') last = null;
      let gear;
      let con = null;
      for (let d of data) {
        if (Array.isArray(d)) {
          let gg = create(d, last);
          //if (last && con) {
          //}
        } else if (typeof d === 'object') {
          gear = create(d);
          if (last && con) {
            connect(last, gear, con);
            driverGears = driverGears.filter(e=>e!=gear);
          }
          last = gear;
          con = null;
        } else if (typeof d === 'string') {
          con = d;
        }
      }
      return;
    }

    if (typeof data === 'object') {
      let teeth = data.teeth || data.t;
      let pos = data.pos || {x: data.p[0], y: data.p[1]};
      let color = data.color || data.c;
      let gear = new Gear(p, teeth, pos, color);
      allGears.push(gear);
      driverGears.push(gear);
      gear.setSpeed(1);
      return gear;
    }

  }


  function getConnectionName(c, useSmallProps) {
    if (c instanceof ChainedGearConnection) {
      return useSmallProps ? "c" : "chained";
    }
    if (c instanceof CrossChainedGearConnection) {
      return useSmallProps ? "x" : "crossed";
    }
    if (c instanceof StickyGearConnection) {
      return useSmallProps ? "s" : "sticky";
    }
    if (c instanceof TouchingGearConnection) {
      return useSmallProps ? "t" : "touching";
    }
    if (c instanceof MagicConnection) {
      return useSmallProps ? "m" : "magic"; // TODO
    }
    return "";
  }


  function describeGears(gear, useSmallProps) {
    let data = [];

    if (typeof gear === "undefined" || gear === null) {
      for (let g of driverGears) {
        let result = describeGears(g, useSmallProps);
        data = data.concat(result);
      }
      return data;
    }

    data.push(gear.describe(useSmallProps))
    if (gear.connections.length === 1) {
      data.push(getConnectionName(gear.connections[0], useSmallProps));
      let result = describeGears(gear.connections[0].getGear2(), useSmallProps)
      data = data.concat(result);
    } else if (gear.connections.length > 1) {
      let cons = [];
      for (let c of gear.connections) {
        let d = [getConnectionName(c, useSmallProps)];
        let result = describeGears(c.getGear2(), useSmallProps)
        d = d.concat(result);
        cons.push(d);
      }
      data.push(cons);
    }
    return data;
  }

  async function saveToStorage() {
    let data = describeGears(null, false);
    await p.storeItem('gears', data);
  }

  function getDataFromStorage() {
    let data = p.getItem("gears");
    if (!data) return null;
    if (!Array.isArray(data)) data = null;
    if (Array.isArray(data) && data.length === 0) data = null;
    return data;
  }
  
  function getDataFormURL() {
    let url = new URL(window.location);
    //url = new URL("http://127.0.0.1:5500/?[%22sdfsdfsdf%22,{%22d%22:34}]");
    //url = new URL("http://127.0.0.1:5500/");
    let data = decodeURIComponent(url.search.substring(1))
    if (!data) return null;
    data = JSON.parse(data);
    if (!Array.isArray(data)) data = null;
    if (Array.isArray(data) && data.length === 0) data = null;
    return data;
  }

  async function maybeLoadURLData() {
    let urlData = getDataFormURL();
    if (urlData) {

      if (allGears.length > 0) {
        if (confirm(`Du rufst ein Experiment über URL auf.
Im Speicher ist noch ein anderes Experiment.
Möchtest du das zuletzt verwendete Experiment zuvor herunterladen?`)) {
          console.log("TODO herunterladen von storageData");
          await downloadCurrentExperiment();
        }
      }
  
      driverGears = [];
      allGears = [];
      await saveToStorage();
  
      console.log('Lade URL-Experiment', urlData)
      create(urlData);
      saveToStorage();
    } else if (allGears.length === 0) {
      loadExample();
    }

  }


  async function downloadCurrentExperiment() {
    var data = JSON.stringify(describeGears());
    var c = document.createElement("a");
    c.download = "gears.txt";
    var t = new Blob([data], {type: "text/plain"});
    c.href = window.URL.createObjectURL(t);
    c.click();
  }



  function loadExample() {
    console.log('Lade Beispiel-Experiment')
    create([
      { teeth: 12, pos: {x: 70, y: 100}, color: '#ff5050' },
      'chained',
      { teeth: 20, pos: {x: 300, y: 180}, color: '#50ff30' },
    ]);
  }

  p.setup = function() {
    let m = document.getElementById('main');
    renderer = p.createCanvas(m.offsetWidth, m.offsetHeight);

    initCanvasEventListeners();
    initToolbar();
    p.frameRate(frameRate);

    let storageData = getDataFromStorage();
    if (storageData) {
      console.log('Lade Storage-Experiment', storageData)
      create(storageData);
    }

    setTimeout(maybeLoadURLData, 200);

    setTimeout(()=>p.windowResized(), 100);

  };

  p.windowResized = () => {
    let m = document.getElementById('main');
    p.resizeCanvas(m.offsetWidth, m.offsetHeight);
  };


  p.draw = function() {
    p.background(isDark ? 40 : 200);

    for (let gr of driverGears) {
      gr.draw();
      // roter kreis für antriebsrad
      p.fill(200, 10, 10);
      let pos = gr.getPosition();
      p.circle(pos.x, pos.y, 20);
      if (showConnectionValue || showGearValue) {
        // beschriftung
        gr.drawRatios(p, new Bruch(1,1), showGearValue, showConnectionValue);
      }
    }

    for (let selectedGear of selectedGears) {
      if (selectedGear == null) continue;
      let pos = selectedGear.getPosition();
      let r = selectedGear.getOuterRadius();
      p.stroke(255,255,10,70);
      p.strokeWeight(10);
      p.noFill();
      p.circle(pos.x, pos.y, r*2);
      /*if (selectedGear.from != null) {
        let other = selectedGear.from.getGear1();
        let p2 = other.getPosition();
        p.strokeWeight(2);
        p.circle(p2.x, p2.y, other.getOuterRadius()*2);
        if (!(selectedGear.from instanceof StickyGearConnection)) {
          let d = p.dist(pos.x, pos.y, p2.x, p2.y);
          let d1 = (r+5)/d;
          let d2 = (d-other.getOuterRadius()-1)/d;
          p.line(pos.x+d1*(p2.x-pos.x), pos.y+d1*(p2.y-pos.y), pos.x+d2*(p2.x-pos.x), pos.y+d2*(p2.y-pos.y));
        }
      }*/
    }
    /*if (currentCommand != null && currentParam != null) {
      g.fill(255, 50);
      g.noStroke();
      g.rectMode(CORNER);
      g.rect(0, height-50, width, 50);
      currentParam.draw(g, 10, height-40);
    }*/
    if (rotationMethod == "step") {
      //stepPerSecond(60);
    } else if (rotationMethod == "revolution") {
      rotationRPM(revolutionsPerMinute);
    }
    //Timer.tick();
  };




  function gearHitTest(parent, point) {
    let sticky = [];
    
    for (let c of parent.connections) {
      let g2 = c.getGear2();
      if (c instanceof StickyGearConnection) {
        sticky.push(g2);
      }
    }
    if (sticky.length>0) {
      sticky.push(parent);
      let sg = null;
      let r = 0;
      for (let g of sticky) {
        if (isPointOnGear(point, g)) {
          if (sg == null || g.getRadius() < r) {
            r = g.getRadius();
            sg = g;
          }
        }
      }
      if (sg != null) return sg;
    }
    
    if (isPointOnGear(point, parent)) {
      return parent;
    }
    
    for (let c of parent.connections) {
      let g2 = c.getGear2();
      let sg = gearHitTest(g2, point);
      if (sg != null) return sg;
    }
    
    return null;
  }
  
  function isPointOnGear(point, gear) {
    let pos = gear.getPosition();
    let r = gear.getOuterRadius();
    let d = p.dist(pos.x, pos.y, point.x, point.y);
    return d < r;
  }
  


  function selectNothing() {
    selectedGears[0] = null;
    selectedGears[1] = null;
  }

  function select(g, clear) {
    if (clear) {
      selectedGears[0] = g;
      selectedGears[1] = null;
      return;
    }
    if (selectedGears.includes(g)) return;
    if (selectedGears[0] == null) {
      selectedGears[0] = g;
    } else if (selectedGears[1] == null) {
      selectedGears[1] = g;
    }
  }

  function selectionFull() {
    return selectedGears[0] != null && selectedGears[1] != null;
  }

  function getOneSelected() {
    return selectedGears[0];
  }

  function exactlyOneSelected() {
    return selectedGears[0] != null ^ selectedGears[1] != null;
  }

  function promptForInt(title, def) {
    let input = prompt(title, ""+def);
    if (input === null) return null;
    let n = Math.abs(parseInt(input));
    if (isNaN(n)) {
      return null;
    }
    return n;
  }

  function promptForString(title, def) {
    let input = prompt(title, def);
    if (input === null) return null;
    return input;
  }

  function limitTeethCount(c) {
    if (c < 6) c = 6;
    if (c > 60) c = 60;
    return c;
  }
  
  function initToolbar() {
    let nav = document.body.querySelector('nav');

    let events = {
      'clear': _(()=>{
        if (!confirm("Alles löschen?")) return;
        driverGears = [];
        allGears = [];
        selectNothing();
        saveToStorage();
      }),
      'load': _(async ()=>{
        function openFile() {
          return new Promise((res, rej)=>{
            function readFile(file) {
              return new Promise((resolve, reject) => {
                let freader = new FileReader();
                freader.onload = x=> resolve(freader.result);
                freader.readAsText(file);
            })}
            async function readInputFile(event) {
              let data = await readFile(this.files[0]);
              res(JSON.parse(data));
            }
            let el = document.createElement('input');
            el.setAttribute('type', "file");
            el.onchange = readInputFile;
            el.click();
          })
        }

        let data = await openFile();
        if (Array.isArray(data)) {
          driverGears = [];
          allGears = [];
          create(data);
          saveToStorage();
        }

      }),
      'save': _(()=>{
        downloadCurrentExperiment();
      }),




      'add': _(() => {
        let teeth = promptForInt("Wieviele Zähne?", 12);
        if (teeth == null) return;
        let last;
        let selectedGear = getOneSelected();
        if (selectedGear != null) {
          last = selectedGear;
        } else if (allGears.length > 0) {
          last = allGears[allGears.length-1];
        } else {
          last = null;
        }
        // connection TODO
        let g = newGear(teeth, last, null);
        saveToStorage();
      }),
      //'add2': _(() => {}),
      'remove': _(() => {
        if (!exactlyOneSelected()) return;
        let selectedGear = getOneSelected();
        if (selectedGear.connections.length !== 0) return;
        driverGears = driverGears.filter(e=>e!=selectedGear);
        allGears = allGears.filter(e=>e!=selectedGear);
        let fromConnection = selectedGear.from;
        if (fromConnection != null) {
          let g1 = fromConnection.getGear1();
          g1.disconnect(fromConnection);
        }
        selectNothing();
        saveToStorage();
      }),
      'teeth': _(() => {
        if (!exactlyOneSelected()) return;
        let selectedGear = getOneSelected();
        let teeth = promptForInt("Wieviele Zähne?", selectedGear.getTeethCount());
        if (teeth == null) return;
        teeth = limitTeethCount(teeth);
        selectedGear.setTeethCount(teeth);
        saveToStorage();
      }),
      'color': _(() => {
        if (!exactlyOneSelected()) return;
        let selectedGear = getOneSelected();

        let colorPicker = p.createColorPicker(selectedGear.originalColor);
        colorPicker.position(-100, -100);
        colorPicker.elt.click();
        //colorPicker.changed((e)=>console.log("c", e))
        colorPicker.input((e)=>{
          selectedGear.setColor(colorPicker.value());
          saveToStorage();
        })
        /*
        let color = promptForString("Welche Farbe?", selectedGear.originalColor);
        if (color === null) return;
        selectedGear.setColor(color);*/
      }),

      'cross': createChangeConnectionHandler('x'),
      'chain': createChangeConnectionHandler('c'),
      'touch': createChangeConnectionHandler('t'),
      'magic': createChangeConnectionHandler('m'),
      'sticky':createChangeConnectionHandler('s'),
      'none':  createChangeConnectionHandler('n'),
      
      'rotate': createCarusselButtonHandler({"revolution": 1,"": 0, "hand": 2}[rotationMethod], (c) => {rotationMethod = {0: "", 1: "revolution", 2: "hand"}[c]}),
      'loop': createCarusselButtonHandler(p.isLooping() ? 0 : 1, (c) => {c==0 ? p.loop() : p.noLoop()}),
      'reset': _(() => {
        resetRotation();
      }),
      'revolution': _(() => {
        let n = promptForInt("Wieviele Umdrehungen pro Minute?", ""+revolutionsPerMinute);
        if (n == null) return;
        if (n<1) n=1;
        if (n>60) n=60;
        revolutionsPerMinute = n;
        resetRotation();
      }),

      'gearvalue': createCarusselButtonHandler(showGearValue ? 0 : 1, (c) => {showGearValue = c == 0 ? true : false}),
      'connectionvalue': createCarusselButtonHandler(showConnectionValue ? 0 : 1, (c) => {showConnectionValue = c == 0 ? true : false}),

      'help': _(()=>{
        let dialog = document.getElementById('helpDialog');
        dialog.showModal();
      }),
      'link': _(()=>{
        let data = JSON.stringify(describeGears(null, true));
        let url = new URL(window.location);
        url.search = '?'+data;
        console.log(url.toString());
        prompt("Link:", url.toString())
      }),
    }

    function resetRotation() {
      startMillis = p.millis();
      Gear.rotation = 0;
    }

    function createCheckButtonHandler(def, cb) {
      let state = def;
      return (el)=>{
        function u() {
          state ? el.classList.add('checked') : el.classList.remove('checked')
        }
        u();
        return () => {
          state = !state;
          cb(state);
          u();
        }
      }
    }

    function createCarusselButtonHandler(def, cb) {
      let state = def;
      return (el)=>{
        let count = el.children.length;
        function u() {
          for (let i = 0; i<count; i++) {
            el.children.item(i).style.display = state == i ? 'inline' : 'none';
          }
        }
        u();
        return () => {
          state++;
          if (state == count) state = 0;
          cb(state);
          u();
        }
      }
    }

    function createChangeConnectionHandler(con) {
      return _(function() {
        if (!selectionFull()) return;
        connectTwoGears(selectedGears[0], selectedGears[1], con);
      })
    }

    function _(cb) {
      return (el)=>cb;
    }
    

    for (let e in events) {
      let el = nav.querySelector('button[name='+e+']');
      if (el == null) continue;
      el.addEventListener('click', events[e](el));
    }

  }
  
  function initCanvasEventListeners() {

    function pposFromTouch(e) {
      let rp = renderer.position();
      let t = e.targetTouches[0]
      if (typeof t === 'undefined') return null;
      return {x: t.clientX-rp.x, y: t.clientY-rp.y}
    }
    function pposFromP5() {
      return {x: p.mouseX, y: p.mouseY}
    }

    let moved = false;
    function mp(e, c) {
      let mouse = e.pointerPosition;
      moved = false;
      for (let g of driverGears) {
        let sg = gearHitTest(g, mouse);
        if (sg != null) {
          select(sg, c);
          let pos = sg.getPosition();
          selDist = {x: mouse.x-pos.x, y: mouse.y-pos.y};
          return;
        }
      }
      selectNothing();
    }
    let ttime = null;
    renderer.touchStarted(function(event) {
      event.pointerPosition = pposFromTouch(event);
      event.preventDefault();
      let c = !(ttime == null || ttime>p.millis());
      ttime = p.millis()+900;
      mp(event, c)
      return false;
    });
    renderer.mousePressed(function(event) {
      event.pointerPosition = pposFromP5();
      event.preventDefault();
      mp(event, !(event.shiftKey || event.ctrlKey))
      return false;
    });


    function mm(e) {
      if (exactlyOneSelected()) {
        let mouse = {x: p.mouseX, y: p.mouseY};
        let movingGear = getOneSelected();
        if (movingGear.from instanceof StickyGearConnection) {
          movingGear = movingGear.from.getGear1();
        }
        if (rotationMethod === "hand" && driverGears.includes(movingGear)) {
          let v1 = p.createVector(mouse.x, mouse.y);
          let v2 = p.createVector(selDist.x, selDist.y);
          let pos = movingGear.getPosition();
          v1.sub(pos.x, pos.y);
          let angle = v1.angleBetween(v2);
          if (isNaN(angle)) return;
          Gear.rotation -= angle;
          if (Gear.rotation<0) Gear.rotation = 0;
          selDist = v1;
        } else {
          let pos = {x: mouse.x-selDist.x, y: mouse.y-selDist.y};
          movingGear.setPosition(pos);
          moved = true;
        }
      }
    }
    renderer.touchMoved(function(event) {
      event.pointerPosition = pposFromTouch(event);
      event.preventDefault();
      mm(event);
      return false;
    });
    renderer.mouseMoved(function(event) {
      event.pointerPosition = pposFromP5();
      event.preventDefault();
      if (event.buttons == 0) return false;
      mm(event);
      return false;
    });

    renderer.mouseReleased(function(event) {
      if (moved) {
        saveToStorage();
      }
    })
    renderer.touchEnded(function(event) {
      if (moved) {
        saveToStorage();
      }
    })
  }


  function findPosition(g1, g2) {
    let pos1;
    let r1;
    let r2 = g2.getOuterRadius();
    if (g1 != null) {
      pos1 = g1.getPosition();
      r1 = g1.getOuterRadius();
    } else {
      pos1 = {x: p.width/2, y: p.height/2};
      r1 = -r2; // hack
    }
    let r = (r1+r2)*0.9;
    for (let i=0; i < 10; i++) {
      let positions = [
        {x: pos1.x+r+20, y:     pos1.y},           // #>
        {x: pos1.x+r,    y:  pos1.y-r},            // #'
        {x: pos1.x+r,    y:  pos1.y+r},            // #,
        {x: pos1.x+1,    y:    pos1.y-r+20},       // ^
        {x: pos1.x+1,    y:    pos1.y+r+20},       // v
        {x: pos1.x-r,    y:  pos1.y+r},            // ,#
        {x: pos1.x-r,    y:  pos1.y-r},            // '#
        {x: pos1.x-r+20, y:     pos1.y},           // <#
      ];
      for (let pos2 of positions) {
        if (pos2.y<topSpace) pos2.y=topSpace;
        if (testPosition(pos2, r2)) {
          if (pos2.y<topSpace) pos2.y=topSpace;
          g2.setPosition(pos2);
          return;
        }
      }
      r += 10;
    }
  }
  
  function testPosition(pos, r) {
    let x1 = pos.x - r, y1 = pos.y - r, x2 = pos.x + r, y2 = pos.y + r;
    let gx1, gy1, gx2, gy2;
    if (x1 < 0 || x2 > p.width || y1 < topSpace || y2 > p.height) return false;
    for (let g of allGears) {
      let gpos = g.getPosition();
      let gr = g.getOuterRadius()*1.3;
      gx1 = gpos.x - gr;
      gy1 = gpos.y - gr;
      gx2 = gpos.x + gr;
      gy2 = gpos.y + gr;
      if (gx1 > x2 || gy1 > y2 || gx2 < x1 || gy2 < y1) continue;
      return false;
    }
    return true;
  }

  function newGear(teeth, g1, connection) {
    let g2;
    g2 = new Gear(p, teeth, null, isDark ? 'white' : 'black');
    g2.setPosition({x: p.width/2, y: p.height/2});
    if (driverGears.length == 0) {
      g2.setSpeed(1);
    }
    allGears.push(g2);
    driverGears.push(g2); // nur wenn keine connection
    g2.setSpeed(1);
    findPosition(g1, g2);
    return g2;
  }
    



  function connectTwoGears(g1, g2, con, p) {
    if (g2.from != null && g2.from.getGear1() == g1) {
      console.log("miteinander verbunden normale ausrichtung -> connection ändern");
    } else if (g1.from != null && g1.from.getGear1() == g2) {
      console.log("miteinander verbunden aber verdrehte ausrichtung -> g1 und g2 tauschen, dann connection ändern");
      let g = g1;
      g1 = g2;
      g2 = g;
    } else {
      console.log("nicht miteinander verbunden");
      if (g2.from != null && g1.from != null) {
        console.log("beide anderweitig verbunden -> exit");
        return;
      } else if (g1.from == null && g2.from != null) { // wenn beide null ist die reihenfolge des markierens bedeutsam
        console.log("g1 hat keinen from und sollte daher g2 sein");
        let g = g1;
        g1 = g2;
        g2 = g;
      }
      if (isCircular(g1,g2) || isCircular(g2,g1)) { // muss man die auch tauschen?
        console.log("zirkuläre verbindung! --> exit");
        return;
      }
    }
    
    console.log("connection ändern!");
    
    if (g2.from != null) {
      g1.disconnect(g2.from);
      g2.from = null;
      driverGears.push(g2); // steht jetzt erstmal alleine
      g2.setSpeed(1);
    }
    doConnection(g1, g2, con, p);
    if (g2.from != null) {
      driverGears = driverGears.filter(e=>e!=g2);
    }
    saveToStorage();
  }
  
  function isCircular(g1, g2) {
    if (g1 == g2) return true;
    // g1 bleibt constant
    if (g2.from == null) return false;
    let next = g2.from.getGear1();
    return isCircular(g1, next);
  }
  
  function doConnection(g1, g2, con, p) {
    if (con === "t") {
      g1.connectTouching(g2);
      return;
    }
    if (con === "c") {
      g1.connectChained(g2);
      return;
    }
    if (con === "x") {
      g1.connectCrossChained(g2);
      return;
    }
    if (con === "s") {
      g1.connectSticky(g2);
      return;
    }
    if (con === "m") {
      let b = new Bruch(1,1); // TODO
      g1.connectMagic(g2, b);
      return;
    }  
  }
  
  /*
  void changeConnectionCommand(String con, String p) {
    // die verbinding wo der antrieb her kommt, denn die ist eindeutig!
    if (!exactlyOneSelected()) return;
    Gear selectedGear = getOneSelected();
    GearConnection c = selectedGear.from;
    if (c == null) return;
    Gear g1 = c.getGear1();
    g1.disconnect(c);
    selectedGear.from = null;
    if (con.equals("none")) {
      drivers.add(selectedGear); // steht jetzt alleine
      selectedGear.setSpeed(1);
      return;
    }
    doConnection(g1, selectedGear, con, p);  
    if (selectedGear.from != null) {
      drivers.remove(selectedGear);
    }
  }*/
  

};

new p5(s);