import processing.core.PVector;
import processing.core.PGraphics;

class ClockHand extends Gear {
  float length;
  float width;
  
  public ClockHand(int teethCount, PVector pos, float length, float width, int c) {
    super(teethCount, pos, c);
    this.length = length;
    this.width = width;
  }
  
  
  protected void drawMe(PGraphics g) {
    //super.drawMe(g);
    
    float centerPositionX = this.p.x;
    float centerPositionY = this.p.y;
    float rotationAngle = Gear.rotation;
    
    g.pushMatrix();
    g.rectMode(PGraphics.CORNER);
    g.translate(centerPositionX, centerPositionY);
    g.rotate( this.speed*rotationAngle+PGraphics.PI);
    
    g.fill(this.color);
    g.rect(-this.width/2, 0, this.width, this.length);
    g.circle(0, 0, this.width*1.4f);
    
    g.popMatrix();
  }
  
  
  protected void drawMyRatio(PGraphics g, int startZ, int startN) {
  }
 
  
}
