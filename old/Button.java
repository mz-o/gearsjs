import processing.core.PVector;
import processing.core.PGraphics;
import processing.core.PApplet;
import processing.core.PShape;
import java.util.ArrayList;
//import processing.app.SketchException;

class Button {
  static PApplet app;
  
  static float size = 50;
  /*static ArrayList<Button> buttons = new ArrayList<Button>();
  static void add(Button b) {
    buttons.add(b);
  }
  static void paint_all(PGraphics g) {
    int x = 0;
    int y = 0;
    for (Button b: buttons) {
      b.paint(g, x, y);
      x+=size;
    }
  }
  static Command hit_test(float mx, float my) {
    float x = 0;
    float y = 0;
    for (Button b: buttons) {
      if (mx>x&&mx<x+size&&my>y&&my<y+size) {
        return b.command;
      }
      x+=size;
    }
    return null;
  }*/
  
  PShape icon;
  Command command;
  char actionKey;
  
  Button(String icon, Command command, char actionKey) {
    this.actionKey = actionKey;
    this.command = command;
    try {
      this.icon = Button.app.loadShape(icon);
    } catch (NullPointerException ex) {
    }
  }
  
  Button(String icon, Command command) {
    this(icon, command, (char)0);
  }
  
  Button(String icon, String command, char actionKey) {
    this(icon, new Command(command), actionKey);
  }
  
  Button(String icon, String command) {
    this(icon, command, (char)0);
  }
  
  void paint(PGraphics g, float x, float y) {
    g.rectMode(PGraphics.CORNER);
    g.fill(255, 255, 255);
    g.stroke(128, 128, 128);
    g.strokeWeight(1);
    g.rect(x, y, Button.size, Button.size);
    if (this.icon != null) {
      g.shape(this.icon, x, y, size, size);
    }
  }
  
  
  
}
