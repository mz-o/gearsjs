

//Simple Gears(visual simulation test code).
//Built with Processing 2.03 by Adrian Fernandez. Miami, FL. USA. (07-31-2014).

// innere tangenten https://www.youtube.com/watch?v=ApEKZ4AfdB4
//             https://www.youtube.com/watch?v=KGtn1VgKIoI
// äußere tangenten https://www.youtube.com/watch?v=cwnZwPZcPxA&list=PLFQhZXGWuTPX3Aw5SfPlHCliVsBt5ZOLR&index=77
/*


TODO Anderes Zahnrad als antrieb definieren (es darf aber nur eines pro antrieb-einheit sein.
     einige verbindungen müssen dann umgekehrt werden!

TODO magig connection needs param bruch
*/

final int minNumberOfTeeth=3;
final int maxNumberOfTeeth=40;
float r = 0;
int keyTimeer = 0;
boolean receivingParam = false;
String commandString = "";
TimerListener keyTimerListener = null;
Command currentCommand = null;
Param currentParam = null;
//String paramValue = "";
float startMillis = millis();
int revolutionsPerMinute = 10;
//Gear selectedGear = null;
Gear[] selectedGears = new Gear[2];
PVector selDist = null;
float topSpace = 40;
InputListener inputListener;

void setup()
{
  size(1350, 690);  
  frameRate(60);
  textAlign(CENTER);
  textSize(18);
  
  drivers = new ArrayList<Gear>();
  allGears = new ArrayList<Gear>();
   
  Gear.app = this;
  Timer.app = this;
  Button.app = this;
  
  inputListener = new InputListener() {
    void enter() {
      currentCommand.appendParam(currentParam.getValue());
      currentParam = null;
      if (currentCommand.needParam()) {
        currentParam = currentCommand.getCurrentParam();
        currentParam.start();
      } else {
        String cmdString = currentCommand.compileCommand();
        currentCommand = null;
        executeCommand(cmdString);
      }
    }
  };
    
  ButtonGroup.add(new ButtonGroup("Verbindung"))
    .add(new Button("cross.svg", "connect x", 'x'))
    .add(new Button("chain.svg", "connect c", 'c'))
    .add(new Button("touch.svg", "connect t", 't'))
    .add(new Button("magic.svg", "connect m", 'm'))
    .add(new Button("sticky.svg", "connect s",'s'))
    .add(new Button("none.svg", "connect none",'n'))
    ;
        
  ButtonGroup.add(new ButtonGroup("Timing"))
    .add(new Button("reset.svg", "reset"))
    ////.add(new Button("step.svg", "timing step"))
    .add(new Button("revolution.svg",  new Command("timing revolution").addParam("Umdrehungen/min", new TextInput(inputListener))))
    ;
    
  ButtonGroup.add(new ButtonGroup("Zahnräder"))
    .add(new Button("add.svg", new Command("rad").addParam("Zähne", new TextInput(inputListener)), '+'))
    .add(new Button("add2.svg", new Command("bruch").addParam("Bruch", new TextInput(inputListener)), '#'))
    .add(new Button("remove.svg", "delete"))
    .add(new Button("teeth.svg", new Command("teeth").addParam("Zähne", new TextInput(inputListener)), 'z'))
    .add(new Button("color.svg", new Command("color").addParam("Farbe", new ColorInput(inputListener)), 'f'))
    ;
  
  
  
  /*
  Bruch[] ratios = new Bruch[] {
    new Bruch(-8, 12),
    new Bruch(25,10),
    new Bruch(10,14),
    new Bruch(8,23)
  };
  Gear startGear = create(ratios, null);
  drivers.add(startGear);
  startGear.setSpeed(1);
  */
  
  executeCommand("bruch 6/24");
  executeCommand("bruch 1/2");
  
  executeCommand("rad 20");
  executeCommand("bruch 1/2");

}
  

Gear create(Bruch[] ratios, Gear lastGear, String connection) {
  Gear firstGear = null;
  
  //ArrayList<GearPlace> places = new ArrayList<GearPlace>();
  GearPlace place;
  
  float y = height / 2;
  float x = 0;
  
  for (Bruch b: ratios) {
    boolean isPositive = b.isPositive();
    
    // TODO if letzte zahnrad gleich dem nächsten ersten: kein neues zahnrad bauen sonder ndirekt verbinden
    
    Gear g1, g2;
    
    g1 = new Gear(b.zaehler());
    g2 = new Gear(b.nenner());
    g2.setPosition(new PVector(g2.getOuterRadius(), topSpace+g2.getOuterRadius()));
    
    if (connection == null) {
      if (isPositive) {
        g1.connectChained(g2);
      } else {
        g1.connectCrossChained(g2);
      }
    } else {
      if (connection.startsWith("touch") || connection.equals("verzahnt")) {
        g1.connectTouching(g2);
      } else if (connection.startsWith("chain") || connection.equals("kette")) {
        g1.connectChained(g2);
      } else if (connection.startsWith("cross") || connection.startsWith("kreuz")) {
        g1.connectCrossChained(g2);
      } else {
        g1.connectChained(g2); // damit sie auf jeden Fall verbunden sind
      }
    }
    
    if (firstGear == null) firstGear = g1;
    
    if (lastGear != null) {
      lastGear.connectSticky(g1);
      g1.setPosition(lastGear.getPosition());
      // fals g1 größer ist als lastGear, muss die position von lastGear neu berechnet werden
      if (g1.getTeethCount() > lastGear.getTeethCount()) {
        float d = g1.getOuterRadius() - lastGear.getOuterRadius();        
        PVector pos = lastGear.getPosition();
        pos.x += d;
        x += d*2;
        lastGear.setPosition(pos);
      }
    } else {
      // das ist eh das erste
      x += g1.getOuterRadius();
      g1.setPosition(new PVector(x, y));
      x += g1.getOuterRadius();
    }
    allGears.add(g1);
    
    if (lastGear != null) {
      if (g1.getTeethCount() > lastGear.getTeethCount()) {
        findPosition(g1, g2);
      } else {
        findPosition(lastGear, g2);
      }
    } else {
      findPosition(g1, g2);
    }
    allGears.add(g2);
    
    lastGear = g2;
  }
  return firstGear;
}




void revolutionsPerMinute(int rpm) {
  // 1 Umdr. pro Minute
  Gear.rotation += rpm*(TWO_PI/frameRate)/60;   //0.002;
}


void stepPerSecond(int fraction) {
  // Rärdchen alle Sekunde 1/60 weiter
  float m = (int)(millis()-startMillis)/1000;
  Gear.rotation = (TWO_PI/fraction) * m;
}


void draw() {
  background(40);
  PVector pos;
  for (Gear gr : drivers) {
    gr.draw(g);
    // roter kreis für antriebsrad
    fill(200, 10, 10);
    pos = gr.getPosition();
    circle(pos.x, pos.y, 20);
    // beschriftung
    gr.drawRatios(g, new Bruch(1,1));
  }
  for (Gear selectedGear: selectedGears) {
    if (selectedGear == null) continue;
    pos = selectedGear.getPosition();
    float r = selectedGear.getOuterRadius();
    stroke(255,255,10,70);
    strokeWeight(10);
    noFill();
    circle(pos.x, pos.y, r*2);
    if (selectedGear.from != null) {
      Gear other = selectedGear.from.getGear1();
      PVector p2 = other.getPosition();
      strokeWeight(2);
      circle(p2.x, p2.y, other.getOuterRadius()*2);
      if (!(selectedGear.from instanceof StickyGearConnection)) {
        float d = PVector.dist(pos, p2);
        float d1 = (r+5)/d;
        float d2 = (d-other.getOuterRadius()-1)/d;
        line(pos.x+d1*(p2.x-pos.x), pos.y+d1*(p2.y-pos.y), pos.x+d2*(p2.x-pos.x), pos.y+d2*(p2.y-pos.y));
      }
    }
  }
  if (currentCommand != null && currentParam != null) {
    g.fill(255, 50);
    g.noStroke();
    g.rectMode(CORNER);
    g.rect(0, height-50, width, 50);
    currentParam.draw(g, 10, height-40);
  }
  ButtonGroup.paint_all(g);
  if (timingMethod.equals("step")) {
    stepPerSecond(60);
  } else if (timingMethod.equals("revolution")) {
    revolutionsPerMinute(revolutionsPerMinute);
  }
  Timer.tick();
}

boolean executeCommand(Command c) {
  if (c == null) {
    return false;
  }
  currentCommand = c;
  currentCommand.reset();
  if (!currentCommand.needParam()) {
    String commandString = currentCommand.compileCommand();
    if (commandString != null) {
      currentCommand = null;
      executeCommand(commandString);
    }
  } else {
    currentParam = currentCommand.getCurrentParam();
    currentParam.start();
  }
  return true;
}




void mouseReleased() {
}




void keyPressed() {
  if (keyCode == DELETE) {
    executeCommand("delete");
    return;
  }
  if (currentCommand != null && currentParam != null) {
    if (keyCode == ESC) {
      currentCommand.reset();
      currentCommand = null;
      currentParam = null;
      key = 0;
      keyCode = 0;
      return;
    }
    if (keyCode == ENTER) {
      inputListener.enter();
      return;
    }
    if (currentParam != null) {
      if (currentParam.handleKey(key, keyCode)) return;
    }
    return;
  }
  if (executeCommand(ButtonGroup.key_hit_test(key))) return;
}

void resetCommand() {
  receivingParam = false;
  commandString = "";
}

void executeCommand(String command) {
  println("CMD " + command);
  
  Gear selectedGear = getOneSelected();
  
  
  if (command.startsWith("timing ")) {
    command = command.substring(7);
    if (command.startsWith("revolution ")) {
      try {
        command = command.substring(11);
        int number = Integer.parseInt(command);
        revolutionsPerMinute = number;
      } catch (NumberFormatException ex) {
      }
      timingMethod = "revolution";
    } else {
      timingMethod = command;
    }
    executeCommand("reset");
    return;
  }
  
  // add Gear
  if (command.startsWith("bruch ")) {
    command = command.substring(6);
    addCommand2(command.split(" "));
    return;
  }
  
  
  // change color
  if (command.startsWith("color ") && exactlyOneSelected()) {
    command = command.substring(6);
    int c;
    try {
      c = Integer.parseInt(command, 10);
      c = color(red(c), green(c), blue(c), 0x60);
    } catch (NumberFormatException ex) {
      return;
    }
    selectedGear.setColor(c);
    return;
  }
  
}







void addCommand2(String[] cmd) {
  String connection = null;
  String[] sp = cmd[0].split("/");
  if (sp.length < 2) {
    return;
  }
  if (cmd.length >= 2) {
    connection = cmd[1];
  }
  /*int z = limitTeethCount(PApplet.abs(Integer.parseInt(sp[0].trim())));
  int n = limitTeethCount(PApplet.abs(Integer.parseInt(sp[1].trim())));*/
  int z, n;
  try {
    z = PApplet.abs(Integer.parseInt(sp[0].trim()));
    n = PApplet.abs(Integer.parseInt(sp[1].trim()));
  } catch (NumberFormatException ex) {
    return;
  }
  Bruch b = new Bruch(z, n);
  if (b.zaehler() < 6 || b.nenner() < 6) {
    int m = max(ceil(6.0/z), ceil(6.0/n));
    b.erweitern(m);
  }
  Gear last;
  Gear selectedGear = getOneSelected();
  if (selectedGear != null) {
    last = selectedGear;
  } else if (allGears.size() > 0) {
    last = allGears.get(allGears.size()-1);
  } else {
    last = null;
  }
  Bruch[] blist = {b};
  Gear neu = create(blist, last, connection);
  if (drivers.size() == 0) {
    drivers.add(neu);
    neu.setSpeed(1);
  }
}


