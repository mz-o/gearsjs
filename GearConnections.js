
class GearConnection {
  g;
  other;
  constructor(other, g) {
    this.g = g;
    this.other = other;
  }
  getGear1() {
    return this.other;
  }
  getGear2() {
    return this.g;
  }
  drawGears() {
    this.g.draw();
  }
  update() {}
  drawBefore(g) {
  }
  drawAfter(g) {
  }
}











class MagicConnection extends GearConnection {
  ratio;
  
  constructor(other, g, ratio) {
    super(other, g);
    g.showRatio=false;
    this.ratio = ratio.value();
  }
  update() {
    //if (this.g.getTeethCount()==0) return;
    let speed = this.other.getSpeed() * this.ratio;
    this.g.setSpeed(speed);
  }
}



class ChainedGearConnectionBase extends GearConnection {
  crossed;

  constructor(other, g, crossed) {
    super(other, g);
    this.crossed = crossed;
  }
  update() {
    if (this.g.getTeethCount()==0) return;
    let speed =  this.other.getSpeed() * (this.other.getTeethCount() / this.g.getTeethCount());
    if (this.crossed) speed*=-1;
    this.g.setSpeed(speed);
  }
  drawBefore(g) {
    super.drawBefore(g);
    g.push();
    let g1, g2;
    let f = 1;
    if (this.other.getRadius() > this.g.getRadius()) {
      g1 = this.other;
      g2 = this.g;
    } else {
      g2 = this.other;
      g1 = this.g;
      f = -1;
    }
    let radio1 = g1.getRadius();
    let pos1 = g1.getPosition();
    let centerPositionX1 = pos1.x;
    let centerPositionY1 = pos1.y;
    let rotationSpeed1  = g1.getSpeed();
    let radio2 = g2.getRadius();
    let pos2 = g2.getPosition();
    let centerPositionX2 = pos2.x;
    let centerPositionY2 = pos2.y;
    let rotationAngle = Gear.rotation;
    let tp11x,tp11y,tp12x,tp12y,tp21x,tp21y,tp22x,tp22y;
    let helpRadius;
    if (this.crossed) {
      helpRadius = radio1+radio2;
    } else {
      helpRadius = radio1-radio2;
    }
    let abstand = Math.sqrt((centerPositionX1-centerPositionX2)*(centerPositionX1-centerPositionX2) + (centerPositionY1-centerPositionY2)*(centerPositionY1-centerPositionY2));
    if (abstand==0) return;
    let winkel = Math.acos(helpRadius / abstand);
    if (isNaN(winkel)) return; // wenn helpRadius > abstand   dann ist acos nicht definiert
    
    if ((centerPositionX1-centerPositionX2)==0) return;
    let gearWinkel = Math.atan((centerPositionY1-centerPositionY2)/(centerPositionX1-centerPositionX2));
    if (centerPositionX1-centerPositionX2 >= 0) {
      gearWinkel = -(Math.PI-gearWinkel);
    }
    
    let r,w;
    r = Gear.teeth_height*0.2;
    g.stroke(50,105,60);
    g.strokeWeight(4);
    w = winkel+gearWinkel;
    
    
    tp11x = centerPositionX1 + (radio1+r)*Math.cos(w);
    tp11y = centerPositionY1 + (radio1+r)*Math.sin(w);
    if (this.crossed) {
      tp21x = centerPositionX2 + (radio2+r)*Math.cos(w+Math.PI);
      tp21y = centerPositionY2 + (radio2+r)*Math.sin(w+Math.PI);
    } else {
      tp21x = centerPositionX2 + (radio2+r)*Math.cos(w);
      tp21y = centerPositionY2 + (radio2+r)*Math.sin(w);
    }
    w = gearWinkel-winkel;
    tp12x = centerPositionX1 + (radio1+r)*Math.cos(w);
    tp12y = centerPositionY1 + (radio1+r)*Math.sin(w);
    if (this.crossed) {
      tp22x = centerPositionX2 + (radio2+r)*Math.cos(w+Math.PI);
      tp22y = centerPositionY2 + (radio2+r)*Math.sin(w+Math.PI);
    } else {
      tp22x = centerPositionX2 + (radio2+r)*Math.cos(w);
      tp22y = centerPositionY2 + (radio2+r)*Math.sin(w);
    }
    
    g.line(tp11x, tp11y, tp21x, tp21y);
    g.line(tp12x, tp12y, tp22x, tp22y);
    
    g.noFill();
    g.arc(centerPositionX1, centerPositionY1, (radio1+r)*2, (radio1+r)*2,
      (winkel+gearWinkel),
      g.TWO_PI+(-winkel+gearWinkel),
      g.OPEN);
    if (this.crossed) {
      g.arc(centerPositionX2, centerPositionY2, (radio2+r)*2, (radio2+r)*2,
        g.TWO_PI+(gearWinkel-winkel)-g.PI/2,
        g.TWO_PI+(winkel+gearWinkel)+g.PI/2,
        g.OPEN);
    } else {
      g.arc(centerPositionX2, centerPositionY2, (radio2+r)*2, (radio2+r)*2,
        g.TWO_PI+(-winkel+gearWinkel),
        g.TWO_PI+(winkel+gearWinkel),
        g.OPEN);
    }
    
    let umf1 = g.TWO_PI*radio1;
    let a = (tp11x-tp21x)*(tp11x-tp21x) + (tp11y-tp21y)*(tp11y-tp21y);
    let l = g.sqrt(a);
    
    if (l<=0) return;
    if (isNaN(l)) {
      return;
    }
    
    let t = g.map(
      ((rotationAngle* Math.abs(rotationSpeed1))/g.TWO_PI) * umf1, // teil des Umfangs
      0, umf1,
      0, umf1/l); // auf 
    while (t>1) {
      t = t-1;
    }
    
    // TODO I dont know when to do that:
    if (rotationSpeed1<0)
      t = 1-t;
    
    g.fill(50,105,60);
    g.noStroke();
    let dc = Math.floor(l / 15);
    for (let i = 0; i < dc; i++) {
      let tt = (t+(i/dc));
      if (tt>1) tt = tt-1;
      let px = tp12x + tt*(tp22x-tp12x);
      let py = tp12y + tt*(tp22y-tp12y);
      g.ellipse(px, py, 10,10);
    }
    for (let i = 0; i < dc; i++) {
      let tt = ((1-t)+(i/dc));
      if (tt>1) tt = tt-1;
      let px = tp11x + tt*(tp21x-tp11x);
      let py = tp11y + tt*(tp21y-tp11y);
      g.ellipse(px, py, 10,10);
    }
    g.pop();
  }
}


class ChainedGearConnection extends ChainedGearConnectionBase {
  constructor(other, g) {
    super(other, g, false);
  }
}

class CrossChainedGearConnection extends ChainedGearConnectionBase {
  constructor(other, g) {
    super(other, g, true);
  }
}



class StickyGearConnection extends GearConnection {
  update() {
    this.g.setPosition(this.other.getPosition());
    this.g.setSpeed(this.other.getSpeed());
  }
}






class TouchingGearConnection extends GearConnection {
  
  update() {
    if (this.g.getTeethCount()==0) return;
    let speed = -this.other.getSpeed() * (this.other.getTeethCount() / this.g.getTeethCount());
    this.g.setSpeed(speed);
  }
  
  drawBefore(g) {
    super.drawBefore(g);
    g.stroke(255,20);
    g.strokeWeight(5);
    let pos1 = this.getGear1().getPosition();
    let pos2 = this.getGear2().getPosition();
    g.line(pos1.x, pos1.y, pos2.x, pos2.y);
  }
  
}
