class Bruch {
  z;
  n;
  
  constructor(z, n) {
    this.z = z;
    this.n = n;
    
  }
  
  zaehler() {
    return Math.abs(this.z);
  }
  
  nenner() {
    return Math.abs(this.n);
  }
   
  mult(b) {
    this.z *= b.z;
    this.n *= b.n;
  }
  
  erweitern(f) {
    this.z *= f;
    this.n *= f;
  }
  
  kuerzen(f) {
    if (this.z % f == 0 && this.n % f == 0) {
      this.z /= f;
      this.n /= f;
    }
  }
  
  kuerzen() {
    let z = Math.abs(this.z);
    let n = Math.abs(this.n);
    let r = z % n;
     
    while(r>0){
      z = n;
      n = r;
      r = z % n;
    }
    //Kürzen des Bruches
    this.n /= n;
    this.z /= n;
  }
  
  draw(g, pos, c, fs) {
    
    let n = this.nenner();
    let z = this.zaehler();
    let w;

    g.noStroke();
    g.fill(c);
    if (n == 1) {
      fs *= 1.5;
      g.textSize(fs);
      g.textAlign(g.CENTER, g.CENTER);
      let t = ""+z;
      w = g.textWidth(t);
      g.text(t, pos.x, pos.y /*-g.textDescent()*0.5*/);
    } else {
      let nt = "" + n;
      let zt = "" + z;
      g.textSize(fs);
      w = Math.max(g.textWidth(nt), g.textWidth(zt));
      g.textAlign(g.CENTER, g.BOTTOM);
      g.text(zt, pos.x, pos.y);
      g.textAlign(g.CENTER, g.TOP);
      g.text(nt, pos.x, pos.y);
      g.strokeWeight(fs/8);
      g.stroke(c);
      g.line(pos.x-w/2, pos.y-2, pos.x+w/2, pos.y-2);
    }
    if (!this.isPositive()) {
      g.strokeWeight(fs/8);
      g.stroke(c);
      g.line(pos.x-w/2-fs/4, pos.y, pos.x-w/2-fs/4-fs*0.4, pos.y);
    }
  }
  
  value() {
     return this.z / this.n; 
  }
  
  isPositive() {
    return this.value() > 0 ? true : false;
  }
  
  signString() {
    return this.isPositive() ? "" : "–";
  }
  
  sign() {
    return this.isPositive() ? 1 : -1;
  }
  
  
  text() {
    let result = this.zaehler() + "/" + this.nenner();
    if (!this.isPositive()) {
      result = "-"+result;
      //result = "(-"+result+")";
    }
    return result;
    //return "×" + result;
  }
  
  toString() {
    return this.text();
  }
  
  clone() {
    return new Bruch(this.z, this.n);
  }
  
}
