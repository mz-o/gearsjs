


class Gear {
  static rotation = 0;
  static app;
  
  static teeth_height = 8; //18;
  
  g;

  p; // pos
  speed = 0;
  teethCount = 0;
  color = 0;
  originalColor = 0;
  lastTurns = 0;
  
  connections; // ArrayList<GearConnection> 
  from = null;  //GearConnection
  
  showRatio = true;
  
  
  
  constructor(g, teethCount, pos, color) {
    this.g = g;
    this.p = pos;
    this.teethCount = teethCount;
    this.connections = [];
    this.setColor(color);
  }
  
  /*Gear() {
    this(10);
  }
  
  public Gear(int teethCount, PVector pos) {
    this(teethCount, pos, 0x60ffffff);
  }
  
  public Gear(int teethCount) {
    this(teethCount, new PVector(0,0));
  }
  
  public Gear(int teethCount, int color) {
    this(teethCount, new PVector(0,0), color);
  }*/
  
  
  setSpeed(speed) {
    this.speed = speed;
  }
  
  getSpeed() {
    return this.speed;
  }
  
  noRatio() {
    this.showRatio = false;
  }
  
  connectTouching(g) {
    g.from = new TouchingGearConnection(this, g);
    this.connections.push(g.from);
  }
  connectChained(g) {
    g.from = new ChainedGearConnection(this, g);
    this.connections.push(g.from);
  }
  connectCrossChained(g) {
    g.from = new CrossChainedGearConnection(this, g);
    this.connections.push(g.from);
  }
  connectSticky(g) {
    g.from = new StickyGearConnection(this, g);
    this.connections.push(g.from);
  }
  connectMagic(g, ratio) {
    g.from = new MagicConnection(this, g, ratio);
    this.connections.push(g.from);
  }
  /*public void connectNone(Gear g) {
    g.from = new NoGearConnection(this, g);
    this.connections.push(g.from);
  }*/
  
  /*
  public void connectTouching(StickyTwins t) {
    this.connectTouching(t.getGear1());
  }
  public void connectChained(StickyTwins t) {
    this.connectChained(t.getGear1());
  }
  public void connectCrossChained(StickyTwins t) {
    this.connectCrossChained(t.getGear1());
  }*/
  
  /**
   * 
   * @param {GearConnection} c 
   */
  disconnect(c) {
    this.connections = this.connections.filter(e=>e!=c);
  }
  
  disconnectAll() {
    this.connections = [];
  }
  
  
  getTeethCount() {
    return this.teethCount;
  }
  
  setTeethCount(tc) {
    this.teethCount = tc;
  }
  
  getRadius() {
    return this.teethCount * 5;
  }
  
  getOuterRadius() {
    return this.getRadius() + Gear.teeth_height;
  }
  
  getPosition() {
    return this.p;
  }
  
  setPosition(p) {
    if (this.from != null && this.from instanceof TouchingGearConnection) {
      this.p = {x: 40, y: 40}; // TODO ???
    }
    
    this.p = p;
  }
  
  setColor(c) {
    if (typeof c === 'undefined') {
      c = Gear.isDark ? "white" : "black";
    }
    this.originalColor = c;
    this.color = this.g.color(c);
    this.color.setAlpha(120);
    this.lightColor = this.g.lerpColor(this.color, this.g.color(0x66ffffff), 0.3);
  }
  
  
  draw() { //PGraphics
    
    for (let c of this.connections) {
      c.update();
    }
    
    for (let c of this.connections) {
      c.drawBefore(this.g);
    }
    
    this.drawMe();
    
    for (let c of this.connections) {
      c.drawGears();
    }
    
    for (let c of this.connections) {
      c.drawAfter(this.g);
    }
    
    this.lastTurns = this.turns();
  }
  
  drawMe() {
    let g = this.g;
    let radio = this.getRadius();
    let centerPositionX = this.p.x;
    let centerPositionY = this.p.y;
    let teethHeight = Gear.teeth_height;
    let rotationAngle = Gear.rotation;
    
    let numberOfTeeth = this.teethCount; 
    let teethAngle=g.TWO_PI/numberOfTeeth;
    let teethWidth=g.sin(teethAngle/2)*radio; 
    let lineY=g.cos(teethAngle/2)*radio+teethHeight;
    g.push();
    g.translate(centerPositionX, centerPositionY);
    g.rotate( this.speed*rotationAngle);
    
    let c = this.color;
    
    g.fill(c);
    
    g.noStroke();
    for (let i=0; i<numberOfTeeth; i++) {  
      if (i==numberOfTeeth-1) g.fill(this.lightColor);
      else g.fill(c);
      g.rotate(teethAngle); 
      /**
       *      #############3
       *     #              #
       *    #                #
       *   #                  #
       *  1####################2
       */
      g.quad(
      -3*teethWidth/4+2, -lineY+teethHeight+1,
      teethWidth/2+2, -lineY+teethHeight+1,
      teethWidth/4+2, -lineY+1,
      -teethWidth/2+2, -lineY+1
      );
    }
    
    g.fill(c);
    g.noStroke();
    g.ellipse(0, 0, 2*(-lineY+teethHeight), 2*(-lineY+teethHeight)) ;
    
    
    
    g.fill(0);
    g.noStroke();
    g.circle(0, 0, 20);//Shaft
    g.rectMode(g.CENTER);
    g.rect(0, -10, 5, -100/15);
    g.ellipse(0, -0.85*radio, radio/15, radio/15);
    
    
    g.pop();
    
    g.noStroke();
    g.fill(0);
    g.textSize(15);
    g.textAlign(g.CENTER, g.TOP);
    g.text(""+this.turns(), centerPositionX, centerPositionY - radio + Gear.teeth_height);
  }

  
  drawMyRatio(g, bruch) {
    let pos = this.getPosition();
    let c = g.color(255);
    bruch.draw(g, {x: pos.x-1, y: pos.y-1}, c, 28);
    c = g.color(0);
    bruch.draw(g, {x: pos.x+1, y: pos.y+1}, c, 28);
    c = g.color(200, 30,25);
    bruch.draw(g, pos, c, 28);
  }
  
  // startZ ist hier mit Vorzeichen!
  drawRatios(g, start, showGearValue, showConnectionValue) {
    if (this.showRatio && showGearValue) {
      this.drawMyRatio(g, start);
    }
    
    let pos = this.getPosition();
    g.textAlign(g.CENTER, g.CENTER);
    g.textSize(18);
    for (let c of this.connections) {
      let gear = c.getGear2();
      if (!gear.showRatio || c instanceof StickyGearConnection) {
        gear.drawRatios(g, start, showGearValue, showConnectionValue);
      } else {

        let z = this.getTeethCount() * this.signInt(gear.getSpeed()*this.getSpeed());
        let n = gear.getTeethCount();
        let b = new Bruch(z, n);

        if (showConnectionValue) {
          let pos2 = gear.getPosition();
          let mid = {x: (pos.x+pos2.x)/2, y: (pos.y+pos2.y)/2};
          if (Gear.isDark) g.fill(100,200,0);
          else g.fill(10,160,0);
          g.noStroke();
          let s = "×" + b.text() + "\n";
          b.kuerzen();
          s += "×" + b.text();
          g.text(s, mid.x, mid.y);
        }
        
        b.mult(start);
        b.kuerzen();
        gear.drawRatios(g, b, showGearValue, showConnectionValue);
      }
      
    }
    
  }
  
  signInt(n) {
    return n > 0 ? 1 : -1;
  }
  
  turns() {
    return Math.floor(/*Math.abs*/((Gear.rotation/(this.g.TWO_PI/this.getSpeed()))));
  }
  
  newTurnCompleted() {
    return this.turns() != this.lastTurns;
  }
  

  describe(useSmallProps) {
    if (typeof useSmallProps === 'undefined') useSmallProps = false;
    let p = this.getPosition();
    if (useSmallProps) return { t: this.getTeethCount(), p: [Math.round(p.x), Math.round(p.y)], c: this.originalColor };
    return { teeth: this.getTeethCount(), pos: {x: p.x, y: p.y}, color: this.originalColor };
  }
  
}
